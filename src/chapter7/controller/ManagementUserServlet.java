package chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.User;
import chapter7.service.UserService;


@WebServlet(urlPatterns = { "/managementUser" })
public class ManagementUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<User> users = null;
		String account = request.getParameter("account");
		String authority = request.getParameter("authority");
		System.out.println(authority);
		System.out.println(account);

		if (authority.equals("1")) {

			users = new UserService().stopUser(account);
		}else{
			users = new UserService().rebirthUser(account);
		}
		request.getRequestDispatcher("management").forward(request, response);
	}



}
