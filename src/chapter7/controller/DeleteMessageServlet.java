package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.Message;
import chapter7.beans.User;
import chapter7.service.DeleteMessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request,
    		HttpServletResponse response)throws IOException, ServletException{
    	HttpSession session = request.getSession();
    	List<String> deleteMessages = new ArrayList<String>();
    	User user = (User) session.getAttribute("loginUser");
    	
    if (user !=null){
    	
    	Message deleteMessage = new Message();
    	deleteMessage.setId(request.getParameter("message_id"));
    	System.out.println(request);
    	new DeleteMessageService().register(deleteMessage);
    	session.setAttribute("DeleteMessage",deleteMessage);
    	response.sendRedirect("./home");
    	
    	
    }else{
    	session.setAttribute("errorMessages", deleteMessages);
    	response.sendRedirect("home.jsp");
    }
    }
}
    
    
    