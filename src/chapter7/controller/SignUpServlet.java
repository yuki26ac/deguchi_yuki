package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.User;
import chapter7.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setAccount(request.getParameter("account"));
			user.setName(request.getParameter("name"));
			user.setPassword(request.getParameter("password"));
			user.setBranchCode(request.getParameter("branch_code"));
			user.setDepartmentCode(request.getParameter("department"));
			user.setAuthority(request.getParameter("authority"));

			new UserService().register(user);

			response.sendRedirect("./home");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		int branch_code = Integer.parseInt(request.getParameter("branch_code"));
		int department_code = Integer.parseInt(request.getParameter("department_code"));
		String passwordConfirm = request.getParameter("passwordConfirm");
		String branch = String.valueOf(branch_code);
		String department = String.valueOf(department_code);
		String authority = request.getParameter("authority");


		if (StringUtils.isEmpty(account) == true) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (StringUtils.isEmpty(branch) == true) {
			messages.add("支店名を入力してください");
		}
		if (StringUtils.isEmpty(department) == true) {
			messages.add("部署を入力してください");
		}
		if (account.length()<6 || account.length()>20){
			messages.add("アカウント名は6～20文字以下の英数字で設定してください");
		}
		if (!(account.matches("\\W*"))){
			messages.add("アカウント名は半角英数字で設定してください");
		}
		if (name.length() > 10){
			messages.add("名前は10文字以下で設定してください");
		}
	
		if(StringUtils.isEmpty(authority)==true){
			messages.add("ユーザーの状態を入力してください");
		}

		//		if (account.length() < 6 || 20 < account.length() ){
		//			messages.add("ログインIDは6文字以上20文字以下で設定してください");
		//
		//		}else if(!account.matches("[0-9A-Za-z]+$")){
		//			messages.add("ログインIDは半角英数字で設定してください");		
		//			}else {
		//				
		//			}


		if (!(password.equals(passwordConfirm))){
			messages.add("確認用パスワードと一致しません");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
//|| account.matches("[0-9A-Za-z]+$")
//		if (!account.matches("[0-9A-Za-z]+$") ){
//		messages.add("ログインIDは半角英数字で設定してください");		}