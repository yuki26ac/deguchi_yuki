package chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.User;
import chapter7.service.UserService;


@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

	
		List<User> users = new UserService().getResisterUsers();
		request.setAttribute("users", users);
		request.getRequestDispatcher("management.jsp").forward(request, response);
	
}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
//		String account = request.getParameter("account");
////		  HttpSession session = request.getSession();
////	        //セッションよりログインユーザーの情報を取得
////	        User User = (User) session.getAttribute("account");
//	        //ログインユーザー情報のidを元にDBからユーザー情報取得
//	        User editUser = new UserService().getEditUser(account);
//	        request.setAttribute("editUser", editUser);
//
//	        request.getRequestDispatcher("editing.jsp").forward(request, response);
	    }
	
	
}

