package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.service.UserService;

@WebServlet(urlPatterns = { "/editing" })
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int userId=Integer.parseInt(request.getParameter("id"));
		User editUser = new UserService().getEditUser(userId);
		request.setAttribute("editUser", editUser);

		request.getRequestDispatcher("editing.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
		String password = request.getParameter("password");

		if (isValid(request, messages) == true) {

			try {

				if(StringUtils.isEmpty(password)){
					new UserService().updateUser(editUser);
				}else{
					new UserService().update(editUser);}
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("loginUser", editUser);

			response.sendRedirect("./home");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("editing.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setAccount(request.getParameter("account"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranchCode(request.getParameter("branch_code"));
		editUser.setDepartmentCode(request.getParameter("department_code"));
		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {



		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String branch_code =request.getParameter("branch_code");
		String department_code = request.getParameter("department_code");
		String passwordConfirm = request.getParameter("passwordConfirm");
//		String branch = String.valueOf(branch_code);
//		String department = String.valueOf(department_code);		


		if (!(StringUtils.isEmpty(account)) && (account.length()<6 || account.length()>20)){
			messages.add("アカウント名は6～20文字以下の英数字で設定してください");
		}
		if (!(account.matches("^[0-9a-zA-Z]+$"))){
			messages.add("アカウント名は半角英数字で設定してください");
		}


		if  (!(StringUtils.isEmpty(password))&&(password.length() < 6 || 20 < password.length())) {
			messages.add("パスワードは半角6文字以上20文字以下で設定してください");
		}

		if (!StringUtils.isEmpty(name) && name.length() > 10){
			messages.add("名前は10文字以下で設定してください");
		}



		if(StringUtils.isEmpty(name)){
			messages.add("名前を入力してください");
		}

		if(StringUtils.isEmpty(branch_code)){
			messages.add("支店名を入力してください");
		}

		if(StringUtils.isEmpty(department_code)){
			messages.add("部署を入力してください");
		}
		if (!(password.equals(passwordConfirm))){
			messages.add("確認用パスワードと一致しません");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
