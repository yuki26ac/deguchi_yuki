package chapter7.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.UserComment;
import chapter7.beans.UserMessage;
import chapter7.service.CommentService;
import chapter7.service.MessageService;



@WebServlet(urlPatterns = { "/home" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		List<UserMessage> messages = null;

		String searchCategory = request.getParameter("searchCategory");
		String startDate = getStartDate(request);
		String endDate = getEndDate(request);


		if (StringUtils.isEmpty(searchCategory) == false) {
			messages = new MessageService().select(searchCategory);
		} else {
			messages = new MessageService().getMessage();
		}

		if(StringUtils.isEmpty(startDate) == false && StringUtils.isEmpty(endDate)== false){
			messages = new MessageService().search(startDate, endDate);
//			messages = new MessageService().search(endDate);
		}else{
			messages = new MessageService().getMessage();
		}

		List<UserComment> comments = new CommentService().getComment();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("searchCategory", searchCategory);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);



		request.getRequestDispatcher("home.jsp").forward(request, response);
	}
	
	private static String getEndDate(HttpServletRequest req) {
		if (StringUtils.isEmpty(req.getParameter("endDate"))) {
			return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		return req.getParameter("endDate");
	}

	private static String getStartDate(HttpServletRequest req){
		if (StringUtils.isEmpty(req.getParameter("startDate"))){
			return LocalDateTime.of(2018, 07, 01, 00, 00, 0).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
					
//					LocalDateTime.parse("2018-01-01").format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		return req.getParameter("startDate");
	}
	
	
}