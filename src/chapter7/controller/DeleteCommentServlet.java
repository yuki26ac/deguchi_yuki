package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.Comment;
import chapter7.beans.User;
import chapter7.service.DeleteCommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException,ServletException{

		request.getRequestDispatcher("home.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{


		HttpSession session = request.getSession();
		List<String> deleteComments = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");


		if (user !=null){

			
			Comment deleteComment = new Comment();
			deleteComment.setId(request.getParameter("comment_id"));

			new DeleteCommentService().register(deleteComment);
			session.setAttribute("DeleteComment", deleteComment);
			response.sendRedirect("./home");
			
			
	    }else{
	    	session.setAttribute("errorMessages", deleteComments);
	    	response.sendRedirect("home.jsp");
	    }
	    }


		}