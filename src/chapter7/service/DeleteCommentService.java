package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;

import chapter7.beans.Comment;
import chapter7.dao.DeleteCommentDao;

public class DeleteCommentService{
	public void register(Comment deleteComment){
		
		Connection connection = null;
		try{
			connection = getConnection();
			
			DeleteCommentDao deleteCommentDao = new DeleteCommentDao();
			deleteCommentDao.delete(connection,deleteComment);
			
			commit(connection);
	     } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
}