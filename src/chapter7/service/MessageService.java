package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter7.beans.Message;
import chapter7.beans.UserMessage;
import chapter7.dao.MessageDao;
import chapter7.dao.SearchCategoryDao;
import chapter7.dao.SearchDateDao;
import chapter7.dao.UserMessageDao;


public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//    private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage() {

		Connection connection = null;

		try {
			connection = getConnection();

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection);    		
			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	private static final  String searchCategory;
	
	public List<UserMessage> select(String searchCategory){
		Connection connection = null;
		try{
			connection = getConnection();

			SearchCategoryDao searchCategoryDao = new SearchCategoryDao();
			List<UserMessage> ret = searchCategoryDao.select(connection, searchCategory);
			System.out.println(searchCategory);
			commit(connection);
			System.out.println(ret);
return ret;
		
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> search(String startDate, String endDate){
		Connection connection = null;
		try{
			connection = getConnection();

			SearchDateDao searchDateDao = new SearchDateDao();
			List<UserMessage> ret = searchDateDao.search(connection, startDate, endDate);
		
			commit(connection);
			System.out.println(ret);
return ret;
		
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}