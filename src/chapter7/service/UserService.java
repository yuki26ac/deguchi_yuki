package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter7.beans.User;
import chapter7.dao.UserDao;
import chapter7.utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    
    public User getEditUser(int userId) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		User user = userDao.getEditUser(connection, userId);

    		commit(connection);

    		return user;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
    

    public void update(User user) {

 		Connection connection = null;
 		try {
 			connection = getConnection();

 			String encPassword = CipherUtil.encrypt(user.getPassword());
 			user.setPassword(encPassword);

 			UserDao userDao = new UserDao();
 			userDao.update(connection, user);

 			commit(connection);
 		} catch (RuntimeException e) {
 			rollback(connection);
 			throw e;
 		} catch (Error e) {
 			rollback(connection);
 			throw e;
 		} finally {
 			close(connection);
 		}
 	}

    
    public void updateUser(User user) {

 		Connection connection = null;
 		try {
 			connection = getConnection();

 			

 			UserDao userDao = new UserDao();
 			userDao.update(connection, user);

 			commit(connection);
 		} catch (RuntimeException e) {
 			rollback(connection);
 			throw e;
 		} catch (Error e) {
 			rollback(connection);
 			throw e;
 		} finally {
 			close(connection);
 		}
 	}

    
    
    
    
    
    
    
    
    
    
    
    public List<User> getResisterUsers() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		List<User> ret = userDao.getResisterUsers(connection);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
    
    public List<User> stopUser(String account) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		List<User> ret = userDao.stopUser(connection, account);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
    
    public List<User> rebirthUser(String account) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		List<User> ret = userDao.rebirthUser(connection, account);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
    
    }
