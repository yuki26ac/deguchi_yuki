package chapter7.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.User;

/**
 * Servlet implementation class ManagementUserFilter
 */
@WebFilter(urlPatterns = { "/managemant", "/managementUser","/editing"})
public class ManagementUserFilter implements Filter {

	public static String INIT_PARAMETER_NAME_ENCODING = "encoding";

	public static String DEFAULT_ENCODING = "UTF-8";

	private String encoding;

    @Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
    	
    	HttpSession session = ((HttpServletRequest)request).getSession();
    	User user = (User) session.getAttribute("loginUser");
    
    	String branch_code = request.getParameter("branch_code");
		System.out.println(branch_code);
      
		if (StringUtils.equals(branch_code,"4") ) {
			chain.doFilter(request, response);
		
	}
		else {

		List<String> messages = new ArrayList<String>();
		messages.add("管理者専用のため利用できません");
		session.setAttribute("errorMessages", messages);
		request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	}
    @Override
	public void init(FilterConfig config) {
		encoding = config.getInitParameter(INIT_PARAMETER_NAME_ENCODING);
		if (encoding == null) {
			System.out.println("EncodingFilter# デフォルトのエンコーディング(UTF-8)を利用します。");
			encoding = DEFAULT_ENCODING;
		} else {
			System.out.println("EncodingFilter# 設定されたエンコーディング(" + encoding
					+ ")を利用します。。");
		}
	}
	
	
	@Override
	public void destroy() {
	}

}
