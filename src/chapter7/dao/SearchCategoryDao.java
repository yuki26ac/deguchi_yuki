package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.UserMessage;
import chapter7.exception.SQLRuntimeException;


public class SearchCategoryDao {
	public List<UserMessage> select(Connection connection, String searchCategory){
		PreparedStatement ps = null;
		
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("user_id");
            sql.append(", tittle");
            sql.append(", text");
            sql.append(", category ");
            sql.append("FROM messages where category like");
			sql.append("?");
			
			ps = connection.prepareStatement(sql.toString());
			
			ps.setString(1, "%"+searchCategory+"%");
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
//			System.out.println(getSearchCategory);
//			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
		
		}

	private List<UserMessage> toUserMessageList(ResultSet rs)
		throws SQLException {

	List<UserMessage> ret = new ArrayList<UserMessage>();
	try {
		while (rs.next()) {
			
			int userId = rs.getInt("user_id");
			String category = rs.getString("category");
			String tittle = rs.getString("tittle");
			String text = rs.getString("text");
		

			UserMessage message = new UserMessage();
						
			message.setUserId(userId);
			message.setTittle(tittle);
			message.setCategory(text);
			message.setText(category);
			
			ret.add(message);
		}
		return ret;
	} finally {
		close(rs);
	}
}

}	
		
		
		
		
	
