package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter7.beans.Message;
import chapter7.exception.SQLRuntimeException;

public class DeleteMessageDao {
	
	public void delete(Connection connection, Message deleteMessage){
		
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages where id = ");
			sql.append("?"); 


			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, deleteMessage.getId());
			     
			     

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}


//sql.append("message_id");
//sql.append(",user_id");
//sql.append(", text");
//sql.append(", created_date");
//sql.append(") VALUES (");
// // account
//sql.append(", ?");
//sql.append(", ?");  // text
//sql.append(", CURRENT_TIMESTAMP");// created_date
//sql.append(")");
//ps.setString(1, message.getMessageId());
//ps.setInt(2, comment.getUserId());
//ps.setString(3, comment.getText());
