package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		//preparedstatement 大量のINSERTを実行するような場合など
		//基本的に同じSQL文で挿入する値の部分だけが違うようなSQLを実行する場合

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("account");
			sql.append(", name");
			sql.append(", password");
			sql.append(", branch_code");
			sql.append(", department_code");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(", authority");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // name
			sql.append(", ?"); // password
			sql.append(", ?"); // branch_code
			sql.append(", ?"); // department
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(", ?");
			sql.append(")");
			//string builder+append(文字列を大量につなげる）

			ps = connection.prepareStatement(sql.toString());


			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getBranchCode());
			ps.setString(5, user.getDepartmentCode());
			ps.setString(6, user.getAuthority());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public User getUser(Connection connection, String account,
			String password) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE ");
			sql.append("account = ? ");
			sql.append("AND password = ? ");					
//			sql.append("AND authority = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, account);
			ps.setString(2, password);
//			ps.setString(3, authority);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String name = rs.getString("name");
				String password = rs.getString("password");
				String branch_code = rs.getString("branch_code");
				String department_code = rs.getString("department_code");
				Timestamp created_date = rs.getTimestamp("created_date");
				Timestamp updated_date = rs.getTimestamp("updated_date");
				String authority = rs.getString("authority");

				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setName(name);
				user.setPassword(password);
				user.setBranchCode(branch_code);
				user.setDepartmentCode(department_code);
				user.setCreatedDate(created_date);
				user.setUpdatedDate(updated_date);
				user.setAuthority(authority);


				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<User> getResisterUsers(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id");
			sql.append(",account ");
			sql.append(",name");
			sql.append(",branch_code");
			sql.append(",department_code");
			sql.append(",authority");
			sql.append(" FROM users");


			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toResisterUsersList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toResisterUsersList(ResultSet rs)
			throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String name = rs.getString("name");
				String branch_code = rs.getString("branch_code");
				String department_code = rs.getString("department_code");
				String authority = rs.getString("authority");

				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setName(name);
				user.setBranchCode(branch_code);
				user.setDepartmentCode(department_code);
				user.setAuthority(authority);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	
	
	 public User getEditUser(Connection connection, int userId) {

	        PreparedStatement ps = null;
	        try {
	            String sql = "SELECT * FROM users WHERE id = ?";

	            ps = connection.prepareStatement(sql);
	            ps.setInt(1, userId);
	           

	            ResultSet rs = ps.executeQuery();
	            List<User> userList = toUserList(rs);
	            if (userList.isEmpty() == true) {
	                return null;
	            } else if (2 <= userList.size()) {
	                throw new IllegalStateException("2 <= userList.size()");
	            } else {
	                return userList.get(0);
	            }
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }
	
	

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  account = ?");
			sql.append(", name = ?");
			sql.append(", password = ?");
			sql.append(", branch_code = ?");
			sql.append(", department_code = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getBranchCode());
			ps.setString(5, user.getDepartmentCode());
			ps.setInt(6, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void updateUser(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  account = ?");
			sql.append(", name = ?");
			sql.append(", branch_code = ?");
			sql.append(", department_code = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());

			ps.setString(3, user.getBranchCode());
			ps.setString(4, user.getDepartmentCode());
			ps.setInt(5, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
	
	
	

	public List<User> stopUser(Connection connection, String account){
		List<User> ret = new ArrayList<User>();
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" authority = ?");
			sql.append(" where");
			sql.append(" account = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, 0);
			ps.setString(2, account);
			
			System.out.println(ps);
//			ps.setInt(1, user.getId());
//			ret.add(user);
			ps.executeUpdate();

			return ret;
		
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> rebirthUser(Connection connection, String account){

		List<User> ret = new ArrayList<User>();
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" authority = ?");
			sql.append(" where");
			sql.append(" account = ?");

			ps = connection.prepareStatement(sql.toString());
//			ps.setInt(1, user.getId());
			ps.setInt(1, 1);
			ps.setString(2, account);
			
			System.out.println(ps);
			ps.executeUpdate();
		
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


}
