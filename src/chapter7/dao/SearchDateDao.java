package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.UserMessage;
import chapter7.exception.SQLRuntimeException;


public class SearchDateDao {
	public List<UserMessage> search(Connection connection, String startDate, String endDate){
		
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("tittle");
			sql.append(", category");
			sql.append(", text");
			sql.append(", user_id");
			sql.append(", created_date ");
			sql.append("from messages where ");
			sql.append("created_date between ");
			sql.append("? ");
			sql.append("and");
			sql.append("?");
			
			ps =connection.prepareStatement(sql.toString());
			ps.setString(1, startDate+" 00:00:00");
			ps.setString(2, endDate+" 23:59:59");
			
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


private List<UserMessage> toUserMessageList(ResultSet rs)
		throws SQLException {

	List<UserMessage> ret = new ArrayList<UserMessage>();
	try {
		while (rs.next()) {
			
			int userId = rs.getInt("user_id");
			String category = rs.getString("category");
			String tittle = rs.getString("tittle");
			String text = rs.getString("text");
			Timestamp created_date = rs.getTimestamp("created_date");

			UserMessage message = new UserMessage();
						
			message.setUserId(userId);
			message.setTittle(tittle);
			message.setCategory(category);
			message.setText(text);
			message.setCreatedDate(created_date);
			
			ret.add(message);
		}
		return ret;
	} finally {
		close(rs);
	}
}
}