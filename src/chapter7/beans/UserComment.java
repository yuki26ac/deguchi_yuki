package chapter7.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class UserComment implements Serializable {
    private static final long serialVersionUID = 1L;
//
    private int id;
    private String name;
    private int user_id;
    private String text;
    private String message_id;
    private Timestamp created_date;
    private Date updated_date;
    private String account;
//
public int getId(){
	return id;
}
public void setId(int id){
	this.id= id ;
}
public String getName(){
	return name;
}
public void setName(String name){
	this.name = name;
}
public int getUserId(){
	return user_id;
}
public void setUserId(int user_id){
	this.user_id =user_id;
}
public String getText(){
	return text;
}
public void setText(String text){
	this.text = text;
}
public String getMessageId(){
	return message_id;
}
public void setMessageId(String message_id){
	this.message_id = message_id;
}
public Timestamp getCreatedDate(){
	return created_date;
}
public void setCreatedDate(Timestamp created_date){
	this.created_date = created_date;  
}
public Date getUpdatedDate(){
	return updated_date;
	}
public void setUpdatedDate(Date updated_date){
	this.updated_date = updated_date;
}
public String getAccount() {
	return account;
}
public void setAccount(String account) {
	this.account = account;
	}}

