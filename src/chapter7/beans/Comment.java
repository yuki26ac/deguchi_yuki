package chapter7.beans;


import java.sql.Date;

import javax.servlet.http.HttpServlet;

//commentidをStringに（deltecommentservlet)

public class Comment extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private String id;
	private int user_id;
	private String message_id;
	private String text;
	private Date created_date;


	public String getId() {
		return id;
	}
	public void setId(String string){
		this.id = string;

	}

	public int getUserId(){
		return user_id;
	}
	public void setUserId(int user_id){
		this.user_id = user_id;
	}
	
	public String getMessageId(){
		return message_id;
	}
	
	public void setMessageId(String message_id){
		this.message_id = message_id;
	}
	
	public String getText(){
		return text;
	}
	public void setText(String text){
		this.text = text;
	}
	public Date getCreatedDate(){
		return created_date;
	}
	public void setCreatedDate(Date created_date){
		this.created_date = created_date;
		
	}
}