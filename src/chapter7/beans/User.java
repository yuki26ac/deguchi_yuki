package chapter7.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String account;
    private String name;
    private String password;
    private String branch_code;
    private String department_code;
    private Date created_date;
    private Date updated_date;
    private String authority;
    
    
    public int getId() {
    	return id;
    }
    public void  setId(int id){
    	this.id = id;
    }
    	public String getAccount(){
    		return account;
    	}
    	public void setAccount(String account){
    		this.account = account;
    }
    	public String getName(){
    		return name;
    	}
    	public void setName(String name){
    		this.name = name;
    	}
    	public String getPassword(){
    		return password;
    	}
    	public void setPassword(String password){
    		this.password = password;
    	}
    	public String getBranchCode(){
    		return branch_code;
    	}
    	public void setBranchCode(String branch_code){
    		this.branch_code = branch_code;
    	}
    	public String getDepartmentCode(){
    		return department_code;
    	}
    	public void setDepartmentCode(String department_code){
    		this.department_code = department_code;   		
    	}
    	public Date getCreatedDate() {
    		return created_date;
    	}
    	public void setCreatedDate(Date created_date) {
    		this.created_date = created_date;
    	}
    	public Date getUpdatedDate() {
    		return updated_date;
    	}
    	public void setUpdatedDate(Date updated_date) {
    		this.updated_date = updated_date;
    	}
    	
    	public String getAuthority(){
    		return authority;
    	}
    	public void setAuthority(String authority){
    		this.authority = authority;
    	}
    	
    // getter/setterは省略
}