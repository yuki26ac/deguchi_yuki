package chapter7.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private int userId;
	private String text;
	private String tittle;
	private String category;
	private Date created_date;
	private Date updated_date;
	


	public String getId(){
		return id;
	}
	public void setId(String id){
		this.id= id ;
	}
	public int getUserId(){
		return userId;
	}
	public void setUserId(int userId){
		this.userId =userId;
	}
	public String getText(){
		return text;
	}
	public void setText(String text){
		this.text = text;
	}
	public String getTittle(){
		return tittle;
	}
	public void setTittle(String tittle){
		this.tittle =tittle;
	}
	public String getCategory(){
		return category;
	}
	public void setCategory(String category){
		this.category = category;
	}

	public Date getCreatedDate(){
		return created_date;
	}
	public void setCreatedDate(Date createdDate){
		this.created_date = createdDate;
	}
	public Date getUpdatedDate(){
		return updated_date;
	}
	public void setUpdatedDate(Date updatedDate){
		this.updated_date = updatedDate;
	}


}

//thisの意味