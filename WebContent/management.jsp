<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登録情報の編集</title>
</head>
<body>
	登録情報の編集
	<br />
	<div class="users">
		<table>
			<tr>
				<td>氏名</td>
				<td>アカウント名</td>
				<td>支店</td>
				<td>部署</td>
				<td>アカウントの<br /> 復活/停止
				</td>
				<td>編集</td>
			</tr>




			<c:forEach items="${users}" var="user">
				<tr>
					<td>
						<div class="name">
							<c:out value="${user.name}" />
						</div>

					</td>
					<td><div class="account">
							<c:out value="${user.account}" />
						</div></td>
					<td><div class="branch_code">
							<c:out value="${user.branchCode}" />
						</div></td>
					<td><div class="department_code">
							<c:out value="${user.departmentCode}" />
						</div></td>
					<td><form action="managementUser" method="get">

							<div class="authority">
								<c:out value="${user.authority}" />
								<input type="hidden" name="authority" value="${user.authority}">
								<input type="hidden" name="account" value="${user.account}">
								<input type="submit" value="停止/復活" />
							</div>
						</form></td>
					<td><form action="editing" method="get">
							<input type="hidden" name="id" value="${user.id}"> <input
								type="submit" value="編集" />
						</form></td>

				</tr>

			</c:forEach>




		</table>
		<br />
	</div>
	<form action=signup method=get>
		<input type="submit" value="新規登録">
	</form>
</body>
</html>