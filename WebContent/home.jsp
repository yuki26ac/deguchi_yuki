<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
</head>
<body>
	<div class="profile">
		<div class="name">
			<h2>
				<c:out value="${loginUser.name}" />

				さんようこそ
			</h2>
		</div>
	</div>


	<a href="newpost.jsp">新規投稿</a>
	<br />



	<form action="management" method="get">
		<input type="submit" value="登録情報の管理"> <input type="hidden"
			name="branch_code" value="${loginUser.branchCode}">
	</form>
	<br />
	<br />
	<div class="searchCategories">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="home" method="get">
			カテゴリ検索（部分一致）<br /> <input type="text" name="searchCategory"
				class="searchCategory"
				value='<c:out value="${searchCategory}"></c:out>'></input> <input
				type="submit" value="検索" /> <br /> <br />

			期間指定検索(yyyy-mm-ddの形式で入力)<br /> <input type="text" name="startDate"
				class="searchDate" value='<c:out value="${startDate}"></c:out>'></input>から<input
				type="text" name="endDate" class="searchDate"
				value='<c:out value="${endDate}"></c:out>'></input>まで <br />
			<input type="submit" value="検索" /> <br /> <br />
		</form>
	</div>



	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<div class="account-name">
					<font size=4><span class="account"><c:out
								value="${message.account}" /></span> <span class="name"><c:out
								value="${message.name}" /></span></font>

				</div>
				<div class="tittle">
					タイトル：
					<c:out value="${message.tittle}" />
				</div>
				<div class="category">
					カテゴリー：
					<c:out value="${message.category}" />
				</div>
				<div class="text">
					<font size=5> <c:out value="${message.text}" />
					</font>
				</div>
				<form action="deleteMessage" method="post">
					<c:if test="${loginUser.id==message.userId}">
						<input type="hidden" name="message_id" value="${message.id}">
						<input type="submit" value="投稿の削除" />

						<br />
					</c:if>
				</form>


				<div class="date">
					<fmt:formatDate value="${message.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" />
					<br />
				</div>
			</div>

			<div class="comments">
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
				<div class="comment">
					<form action="newComment" method="post">
						コメントする<br />

						<textarea name="text" cols="100" rows="2" class="tweet-box"></textarea>
						<c:out value="${message.id}" />
						<input type="hidden" name="message_id" value="${message.id}">
						<input type="submit" value="送信" /> <br /> <br />
					</form>
					<c:forEach items="${comments}" var="comment">

						<c:if test="${message.id==comment.messageId}">
							★
								<div class="account-name">
								<span class="account"><c:out value="${comment.account}" /></span>
								<span class="name"><c:out value="${comment.name}" /></span>
							</div>
							<div class="text">
								<c:out value="${comment.text}" />
							</div>

							<div class="date">
								<fmt:formatDate value="${comment.createdDate}"
									pattern="yyyy/MM/dd HH:mm:ss" />
								<br />
							</div>
							<form action="deleteComment" method="post">
								<c:if test="${loginUser.id==comment.userId}">
									<input type="hidden" name="comment_id" value="${comment.id}">
									<input type="submit" value="コメント削除" />
								</c:if>
							</form>
							<br />
						</c:if>
					</c:forEach>
				</div>
			</div>
			<br />
		</c:forEach>
	</div>


	<c:if test="${ not empty loginUser }">

		<a href="logout">ログアウト</a>
	</c:if>

	<br />
	<a href="login.jsp">戻る</a>
</body>
</html>