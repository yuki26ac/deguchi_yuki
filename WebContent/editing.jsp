<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.account}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="editing" method="post">
			<br /> <input name="id" value="${editUser.id}" id="id" type="hidden" />
			<label for="name">名前</label> <input name="name"
				value="${editUser.name}" id="name" /><br /> <label for="account">アカウント名</label>
			<input name="account" value="${editUser.account}" /><br /> <label
				for="password">パスワード</label> <input name="password" type="password"
				id="password" /> <br /> <label for="passwordConfirm">パスワード(確認用)</label>
			<input name="passwordConfirm" type="password" id="passwordConfirm" /><br />
			 <label for="branch_code">支店コード</label> <input
				name="branch_code" value="${editUser.branchCode}" id="email" /> <br />

			<label for="department">部署名</label> <input name="department_code"
				value="${editUser.departmentCode}" id="department_code" /><br /> <input
				type="submit" value="登録" /> <br /> <a href="./">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
	
</body>
</html>